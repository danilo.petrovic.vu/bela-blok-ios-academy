import SwiftUI

struct ResultsView: View {
    @Binding var gameData: GameData
    
    var body: some View {
        VStack{
            ForEach($gameData.games){$score in
                Text("\(String(score.teamA_result)) : \(String(score.teamB_result))")
            }
        }
    }
}
        

