import SwiftUI

struct ScoreView: View {
    @State var score: Score
    var body: some View {
        
            HStack{
                Text(String(score.teamA_result))
                Text(String(score.teamB_result))
            }
    }
}

#Preview {
    ScoreView(score: Score(teamA_result: 142, teamB_result: 20))
}
