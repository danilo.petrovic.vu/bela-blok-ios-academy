import SwiftUI

struct GameOverView: View {
    @Binding var is_game_over: Bool
    
    var body: some View {
        NavigationView {
            VStack{
                Button{
                    is_game_over = false
                } label: {
                    Text("Continue to new game")
                }
                .padding()
                
                NavigationLink{
                    ContentView()
                } label: {
                    Text("Back to main menu")
                }
                .fontWeight(.bold)
                .padding()
            }
        }
    }
}

#Preview {
    GameOverView(is_game_over: Binding.constant(true))
}
