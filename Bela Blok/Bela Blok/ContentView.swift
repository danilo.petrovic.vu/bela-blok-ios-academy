import SwiftUI

struct ContentView: View {
    
    @EnvironmentObject var gameData: GameData
    
    var body: some View {
        NavigationView{
            VStack {
                Spacer()
                
                    HStack{
                        Image("srce")
                            .padding()
                        Image("zir")
                            .padding()
                        Image("zelena")
                            .padding()
                        Image("bundeva")
                            .padding()
                    }
                
                Text("Bela Blok")
                    .font(.title)
                    .foregroundColor(Color.orange)
                
                NavigationLink{
                    PlayView(gameData: Binding.constant(gameData))
                } label: {
                    HStack {
                        Image(systemName: "play.circle")
                        Text("Igraj")
                    }
                }
                .fontWeight(.bold)
                .padding()
                
                NavigationLink{
                    ResultsView(gameData: Binding.constant(gameData))
                } label: {
                    HStack {
                        Image(systemName: "book")
                        Text("Rezultati")
                    }
                }
                .fontWeight(.bold)
                .padding()
                
                Spacer()
            }
        }
    }
}
