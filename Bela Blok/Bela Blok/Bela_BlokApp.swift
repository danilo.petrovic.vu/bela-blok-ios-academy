import SwiftUI

@main
struct Bela_BlokApp: App {
    @StateObject var gameData = GameData()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .task {
                    await gameData.fetchGames()
                }
                .environmentObject(gameData)
        }
        
    }
}
