import SwiftUI

enum Team: String, CaseIterable{
    case team_a = "MI"
    case team_b = "VI"
}

struct Score: Identifiable, Encodable, Decodable{
    var id = UUID().uuidString
    var teamA_result: Int16
    var teamB_result: Int16
}

struct GameView: View {
    @Binding var gameLimit:Int
    @State private var numberInput: String = ""
    @State private var selectedTeam: Team = Team.team_a
    @State private var teamA_result: Int16 = 0
    @State private var teamB_result: Int16 = 0
    @State private var scores: [Score] = []
    @State private var total_result: Int16 = 162
    @State private var game_result: Score = Score(teamA_result: 0, teamB_result: 0)
    @State private var total_game_result: Score = Score(teamA_result: 0, teamB_result: 0)
    @State private var is_game_over: Bool = false
    @Binding var gameData: GameData
    
    var body: some View {
        VStack{
            HStack{
                HStack{
                    VStack{
                        Text("MI")
                    }
                    Spacer()
                    Text(String(teamA_result))
                }
                .padding()
                .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
                
                HStack{
                    VStack{
                        Text("VI")
                    }
                    Spacer()
                    Text(String(teamB_result))
                }
                .padding()
                .border(/*@START_MENU_TOKEN@*/Color.black/*@END_MENU_TOKEN@*/)
            }
            
            Picker("Tim", selection: $selectedTeam){
                ForEach(Team.allCases, id:\.self){ team in Text(team.rawValue)
                    
                }
            }
            .pickerStyle(SegmentedPickerStyle())
            
            TextField("Unesite broj", text: $numberInput)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .keyboardType(.numberPad) // Postavite tipkovnicu na numeričku
            
        HStack {
            Button(action: {
                if selectedTeam == Team.team_a {
                    teamA_result = teamA_result + (Int16(numberInput) ?? 0)
                    teamB_result = total_result - teamA_result
                }
                
                else if selectedTeam == Team.team_b {
                    teamB_result = teamB_result + (Int16(numberInput) ?? 0)
                    teamA_result = total_result - teamB_result
                }
                numberInput=""
            }, label: {
                Text("Unesi")
            })
            
            Button(action: {
                total_result = total_result + (Int16(numberInput) ?? 0)
                if selectedTeam == Team.team_a {
                    teamA_result = teamA_result + (Int16(numberInput) ?? 0)
                }
                
                else if selectedTeam == Team.team_b {
                    teamB_result = teamB_result + (Int16(numberInput) ?? 0)
                }
                numberInput=""
            }, label: {
                Text("Dodaj zvanje")
            })
        }
        Button(action: {
            let score = Score(teamA_result: teamA_result, teamB_result: teamB_result)
            scores.append(score)
            teamA_result = 0
            teamB_result = 0
            numberInput=""
            total_result = 162
            game_result.teamA_result = game_result.teamA_result + score.teamA_result
            game_result.teamB_result = game_result.teamB_result + score.teamB_result
            
            
            if game_result.teamA_result >= gameLimit || game_result.teamB_result >= gameLimit {
                let game = Score(teamA_result: game_result.teamA_result, teamB_result: game_result.teamB_result)
                Task{
                    await gameData.sendGame(game: game)
                }
                if game_result.teamA_result > game_result.teamB_result{
                    total_game_result.teamA_result = total_game_result.teamA_result + 1
                }
                else {
                    total_game_result.teamB_result = total_game_result.teamB_result + 1
                }
                scores.removeAll()
                game_result.teamA_result = 0
                game_result.teamB_result = 0
                is_game_over = true
            }
        }, label: {
            Text("Zavrsi igru")
        })
            
        Text("\(game_result.teamA_result) : \(game_result.teamB_result)")
            Spacer()
        Text("\(total_game_result.teamA_result) : \(total_game_result.teamB_result)")
    }
        .padding()
        .sheet(isPresented: $is_game_over) {
            GameOverView(is_game_over: $is_game_over)
        }
        
    }
}

