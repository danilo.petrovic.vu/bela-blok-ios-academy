import SwiftUI


struct PlayView: View {
    @Binding var gameData: GameData
    
    var body: some View {
        Text("Odaberi igru:")
            .font(.title)
            .foregroundColor(.orange)
            .padding()
        
        NavigationLink{
            GameView(gameLimit: Binding.constant(501), gameData: $gameData)
        } label: {
            Text("501")
                .font(.system(size:50))
        }
        .fontWeight(.bold)
        .padding()
        
        NavigationLink{
            GameView(gameLimit: Binding.constant(701), gameData: $gameData)
        } label: {
            Text("701")
                .font(.system(size:50))
        }
        .fontWeight(.bold)
        .padding()
        
        NavigationLink{
            GameView(gameLimit: Binding.constant(1001), gameData: $gameData)
        } label: {
            Text("1001")
                .font(.system(size:50))
        }
        .fontWeight(.bold)
        .padding()
    }
}
