import Foundation

class GameData: ObservableObject {
    
    let games_url = URL(string: "https://bela-database-default-rtdb.europe-west1.firebasedatabase.app/results.json")!
    
    @Published var games: [Score] = []
    
    func sendGame(game: Score) async
    {
        do {
            let encoder = JSONEncoder()
            encoder.dateEncodingStrategy = .iso8601
            let json = try encoder.encode(game)
            
            
            var request = URLRequest(url: games_url)
            request.httpMethod = "POST"
            request.httpBody = json
            
            let (_, response) = try await URLSession.shared.data(for: request)
            print(response)
        }catch let error {
            print(error)
        }
    }
    
    func fetchGames() async
    {
        do {
            let (data, _) = try await URLSession.shared.data(from: games_url)
            
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            
            let decoded_results = try decoder.decode([String: Score].self, from: data)
            games = [Score](decoded_results.values)
        } catch let error {
            print(error)
        }
    }
}


